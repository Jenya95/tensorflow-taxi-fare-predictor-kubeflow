import psycopg2

PORT = '5432'
HOST = 'cloud-sql-proxy-service.ml-retraining'
DATABASE = 'retraining-db'

def insert_into_training_jobs(user_name, pwd, row):
    try:
        connection = psycopg2.connect(user = user_name,
                                      password = pwd,
                                      host = HOST,
                                      port = PORT,
                                      database = DATABASE)
        cursor = connection.cursor()
        cursor.execute("INSERT INTO training_jobs VALUES ('" + row[0] + "', '" + row[1] + "', " + str(row[2]) + ", " + str(row[3]) + ");")
        connection.commit()
    except Exception as error:
        raise Exception("Error while inserting to training_jobs", error)
    else:
        print('Data successfully inserted!')
    finally:
        cursor.close()
        connection.close()

def insert_into_predictions(user_name, pwd, row):
    try:
        connection = psycopg2.connect(user = user_name,
                                      password = pwd,
                                      host = HOST,
                                      port = PORT,
                                      database = DATABASE)
        cursor = connection.cursor()
        cursor.execute(
            "INSERT INTO predictions VALUES ('" + row[0] + "', " + str(row[1]) + ", " + \
            str(row[2]) + ", " + str(row[3]) + ", " + str(row[4]) + ", " + str(row[5]) + ", " + str(row[6]) + ");")
        connection.commit()
    except Exception as error:
        raise Exception("Error while inserting to predictions", error)
    else:
        print('Data successfully inserted!')
    finally:
        cursor.close()
        connection.close()

def read_first_element(user_name, pwd, query):
    try:
        connection = psycopg2.connect(user = user_name,
                                      password = pwd,
                                      host = HOST,
                                      port = PORT,
                                      database = DATABASE)

        cursor = connection.cursor()
        cursor.execute(query)
    except Exception as error:
        raise Exception("Error while reading from PostgreSQL", error)
    else:
        element = cursor.fetchone()
        return element
    finally:
        cursor.close()
        connection.close()

def read_all_elements(user_name, pwd, query):
    try:
        connection = psycopg2.connect(user = user_name,
                                      password = pwd,
                                      host = HOST,
                                      port = PORT,
                                      database = DATABASE)

        cursor = connection.cursor()
        cursor.execute(query)
    except Exception as error:
        raise Exception("Error while reading from PostgreSQL", error)
    else:
        element = cursor.fetchall()
        return element
    finally:
        cursor.close()
        connection.close()
