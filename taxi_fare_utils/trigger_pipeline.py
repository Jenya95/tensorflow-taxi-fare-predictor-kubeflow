from datetime import datetime
import json
import argparse

import kfp

PIPELINE_NAME = 'taxi-fare-predictor'

def trigger(project_id, bucket_name, train_steps, model_folder, host):
    client = kfp.Client(namespace='default', host=host)
    params = {
        'project_id': project_id,
        'bucket_name': bucket_name,
        'train_steps': train_steps,
        'model_folder': model_folder 
    }
    experiments = client.list_experiments()
    experiment_id = experiments.experiments[0].id
    pipeline_id = client.get_pipeline_id(PIPELINE_NAME)
    job_name = '{}-run-{}'.format(PIPELINE_NAME, str(round(datetime.utcnow().timestamp())))
    try:
        my_run = client.run_pipeline(experiment_id=experiment_id, job_name=job_name, 
                                 pipeline_id=pipeline_id, params=params)
    except Exception as error:
        print(error)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', required=True)
    parser.add_argument('--project_id', required=True)
    parser.add_argument('--bucket_name', required=True)
    parser.add_argument('--train_steps', required=True)
    parser.add_argument('--model_folder', required=True)

    args = parser.parse_args().__dict__
    host = args['host']
    project_id = args['project_id']
    bucket_name = args['bucket_name']
    train_steps = args['train_steps']
    model_folder = args['model_folder']
    trigger(project_id, bucket_name, train_steps, model_folder, host)