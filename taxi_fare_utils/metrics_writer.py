import argparse
import os

from utils import db_connector

def write(input_file, model_name, model_folder):
    user_name = os.environ['USER_NAME']
    pwd = os.environ['PWD']
    try:
        job_name = '{}_{}'.format(model_name, model_folder)
        version_name = 'version_{}'.format(model_folder)
        with open(input_file, 'r') as f:
            lines = f.readlines()
            _, eval_loss, label_mean = lines[-1].split(',')
        eval_loss = float(eval_loss)
        label_mean = float(label_mean)
        row = [job_name, version_name, eval_loss, label_mean]
        db_connector.insert_into_training_jobs(user_name, pwd, row)
    except Exception as error :
        raise Exception(error)
    else:
        print('Metrics successfully inserted!')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--metrics_file_path', required=True)
    parser.add_argument('--model_name', required=True)
    parser.add_argument('--model_folder', required=True)
    args = parser.parse_args().__dict__
    
    metrics_file_path = args['metrics_file_path']
    model_name = args['model_name']
    model_folder = args['model_folder']
    
    write(metrics_file_path, model_name, model_folder)