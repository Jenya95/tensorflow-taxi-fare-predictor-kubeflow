import os
import random
import argparse

import os
from googleapiclient import discovery

from utils import utils, db_connector


def predict_json(model_name, instances):
    """Send json data to a deployed model for prediction.
    """
    service = discovery.build('ml', 'v1')
    name = 'projects/{}/models/{}'.format(os.environ['GCP_PROJECT'], model_name)

    response = service.projects().predict(
        name=name,
        body={'instances': instances}
    ).execute()
    if 'error' in response:
        raise RuntimeError(response['error'])

    return response['predictions']


def sample_model_inputs(nb=2):
    instances = []
    for i in range(nb):
        instance = {'pickuplon': random.uniform(-71, -75),
                    'pickuplat': random.uniform(38, 42),
                    'dropofflon': random.uniform(-71, -75),
                    'dropofflat': random.uniform(38, 42),
                    'passengers': random.randint(1, 10)}
        instances.append(instance)
    return instances


def sample_model_drifted_inputs(nb=2):
    instances = []
    for i in range(nb):
        instance = {'pickuplon': random.uniform(0, 1),
                    'pickuplat': random.uniform(1, 2),
                    'dropofflon': random.uniform(3, 4),
                    'dropofflat': random.uniform(4, -5),
                    'passengers': random.randint(1000, 2000)}
        instances.append(instance)
    return instances

def predict_and_store(model_name, _predictor):
    version_name = utils.get_model_current_version(model_name)
    instances = _predictor()
    predictions = predict_json(model_name, instances)
    rows = []
    user_name = os.environ['USER_NAME']
    pwd = os.environ['PWD']
    for idx, pred in enumerate(predictions):
        input = instances[idx]
        prediction = pred['predictions'][0]
        rows.append((version_name, input['pickuplon'], input['pickuplat'], 
                     input['dropofflat'], input['dropofflon'], input['passengers'], prediction))
    
    for row in rows:
        db_connector.insert_into_predictions(user_name, pwd, row)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_drifted', required=True)
    parser.add_argument('--model_name', required=True)
    args = parser.parse_args().__dict__

    input_drifted = int(args['input_drifted'])
    model_name = args['model_name']
    if input_drifted:
        predict_and_store(model_name, sample_model_drifted_inputs)
    else:
        predict_and_store(model_name, sample_model_inputs)
