import argparse
import os
import numpy as np
from statistics import mean

from utils import utils, db_connector

def is_retraining_needed(model_name, acceptable_gap):
    acceptable_gap = float(acceptable_gap)
    current_version = utils.get_model_current_version(model_name)
    label_mean_query = "SELECT labelMean FROM training_jobs WHERE versionName = '" + current_version + "'"
    predictions_query = "SELECT prediction FROM predictions WHERE versionName = '" + current_version + "'"
    user_name = os.environ['USER_NAME']
    pwd = os.environ['PWD']
    
    try:
        label_mean_res = db_connector.read_first_element(user_name, pwd, label_mean_query)
        label_mean = float(label_mean_res[0])
    except IndexError:  # probably caused by manual training job not persisting metrics 
        label_mean = np.inf
    print('label mean: {} '.format(label_mean))
    _min = label_mean * (1 - acceptable_gap)
    _max = label_mean * (1 + acceptable_gap)
    prediction_res = db_connector.read_all_elements(user_name, pwd, predictions_query)
    if prediction_res:
        prediction_mean = mean([_[0] for _ in prediction_res])
        print('prediction mean: {} '.format(prediction_mean))
        if prediction_mean < _min or prediction_mean > _max:
            with open('/tmp/output', 'w') as f:
                f.write('yes')
        else:
            with open('/tmp/output', 'w') as f:
                f.write('no')
    else:
        with open('/tmp/output', 'w') as f:
            f.write('no')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_name', required=True)
    parser.add_argument('--acceptable_gap', required=True)
    args = parser.parse_args().__dict__

    model_name = args['model_name']
    acceptable_gap = args['acceptable_gap']

    is_retraining_needed(model_name, acceptable_gap)